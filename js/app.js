/** app.js **/

(function(context) {
  'use strict';

  var app = {
    initialize: function initialize() {
      console.log('Application initialisée ...');
    }
  };

  context.app = app;

})(window);